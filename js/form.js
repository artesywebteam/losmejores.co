$.fn.validator.Constructor.INPUT_SELECTOR = ':input:not([type="submit"], button):enabled:visible, .selectpicker';

var paginaweb = 'http://artesyweb.xyz/';

jQuery().ready(function(){
		$('#enviar_email').validator().on('submit', function (e) {
			//e.preventDefault();
			if (e.isDefaultPrevented()) {
				// handle the invalid form...
				return false;
			} else {
				//send notifier
				var form_data = jQuery("#enviar_email").serialize();
				var error = "";
				var automate_page = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
				var page = (paginaweb) ? paginaweb : automate_page;

				jQuery.ajax({
				    type: 'POST',
				    url: 'https://iz590xje3m.execute-api.us-west-2.amazonaws.com/prod/leads',
				    crossDomain: true,
				    data: form_data+"&url="+page,
				    dataType: 'json',
					beforeSend: function(jqXHR,settings){
						jQuery('#boton-enviar').button('loading');
					},
				    success: function(responseData, textStatus, jqXHR) {
				        console.log('POST OK.');
				        window.location.replace("gracias.html");
				    },
				    error: function (responseData, textStatus, errorThrown) {
				        console.log('POST failed.');
						error="&error="+errorThrown+"&url="+window.location.href;
						
						jQuery("#respuesta").html("Ha ocurrido un error. Por favor intentelo de nuevo");
						
				    },
				    complete: function ( jqXHR,  textStatus ){
						jQuery('#boton-enviar').button("reset");
				    }
				});

				return false;
			}

		});

});
