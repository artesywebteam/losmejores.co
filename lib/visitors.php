<?php

$ch = curl_init("https://artesyweb.xyz/subscribe/visitor");

curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,
    http_build_query(array(
        'ip'            => $_SERVER['REMOTE_ADDR'],
        'user_agent'    => $_SERVER['HTTP_USER_AGENT'],
        'page'          => $_POST['page'],
    )));

$server_output = curl_exec ($ch);
curl_close ($ch);

if( $server_output == 'OK')
    header('HTTP/1.1 200 Ok');
else
    header('HTTP/1.1 500 Internal Server Error');
