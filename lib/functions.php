<?php

function send_form_lead($data){
	$ch = curl_init("https://artesyweb.xyz/aw-platform/public/");

	curl_setopt($ch, CURLOPT_POST, 1);	
	curl_setopt($ch, CURLOPT_POSTFIELDS, 
		http_build_query($data));
	
	curl_exec ($ch);
	curl_close ($ch);
}

function send_form_data($url,$datos){
        //extract data from the post
        //set POST variables
        
        $fields = array(
            'lname' => urlencode($datos['last_name']),
            'fname' => urlencode($datos['first_name']),
            'title' => urlencode($datos['title']),
            'company' => urlencode($datos['institution']),
            'age' => urlencode($datos['age']),
            'email' => urlencode($datos['email']),
            'phone' => urlencode($datos['phone'])
        );

        //url-ify the data for the POST
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');
        
        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_USERAGENT      => "spider", // who am i
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER => false     // Disabled SSL Cert checks
        );

        $ch      = curl_init( "https://artesyweb.xyz/aw-registryform/registry.php" );
        curl_setopt_array( $ch, $options );
        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );
    }