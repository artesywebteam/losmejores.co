<?php

    require_once __DIR__ . '/vendor/autoload.php';
    require_once __DIR__ . '/config.php';

    // cargar el motor de plantillas
    $loader = new Twig_Loader_Filesystem(__DIR__ . '/plantillas/');
    $twig = new Twig_Environment($loader, array(
        'cache' => __DIR__ . '/cache/',
      ));

    //cargar el programa de correo
    $mail = new PHPMailer;
    $mail->isMail();
    $mail->CharSet = "UTF-8";
    $mail->isHTML(true);


   // personalizo el idioma
    setlocale(LC_TIME, 'spanish');
    // establecer la zona horaria
    date_default_timezone_set("America/Bogota");

    //funcion para enviar informacion de leads de formularios

    $campos=array();

    foreach ($_POST as $key => $value) {
        $campos[str_replace("_", " ", $key)] = $value;
    }

    $campos['Fecha envio']=strftime("%b %d del %Y - %X");


    if(isset($campos['Email']) && $campos['Email']!=""){
        //si el cliente potencial envió su correo, entonces ponerlo como remitente, para que el cliente pueda responderle facilmente
        $mail->setFrom($campos['Email']);
    }
    else{
        // si el cliente potencial no envió su correo, entonces poner otro correo, por ejemplo el del cliente
        $mail->setFrom("Notificador formulario web");
    }

    // modificar datos de aca
    // agregar destinatario
    $mail->Subject=$asunto;

    // poner todos los correos del cliente en la direccion del correo a enviar
    foreach ($correos_clientes as $correo) {
        $mail->addAddress($correo);
    }

    // poner todos los correos copia en la direccion de copia oculta del correo a enviar
    foreach ($correos_copias as $correo) {
        $mail->addBCC($correo);
    }

    if(isset($campos['error']) && $campos['error'] != ""){

        $error = $campos['error'];
        $url = $campos['url'];
        unset($campos['error']);
        unset($campos['url']);

        $m = new PHPMailer;
        $m->isMail();
        $m->CharSet = "UTF-8";
        $m->isHTML(true);

        foreach ($correos_error as $correo) {
            $m->addBCC($correo);
        }

        $m->Body = $twig->render('error_sistema.html', array( 'campos' => $_POST, 'error' => $error, 'url' => $url ));

        $m->send();

    }

    // compongo el cuerpo, debe hacerse las modificaciones en el archivo plantillas/notificacion.html
    $mail->Body = $twig->render('notificacion.html', array( 'campos' => $campos));

    // enviar correo
    if(!$mail->send()){
        header("X-Error-Message: ".$mail->ErrorInfo, true, 500);
    }
    else{
        header("HTTP/1.0 200 OK", true, 200);
    }

    exit;

    //send_form_lead($campos);

?>
